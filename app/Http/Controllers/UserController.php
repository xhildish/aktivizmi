<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Candidat;
use App\Models\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
class UserController extends Controller
{
    public function fileUpload(Request $req)
    {
        $id = $req->query('id');

        $req->validate([
            'file' => 'required|mimes:jpg,png,jpeg,pdf|max:2048'
        ]);

            if ($req->file()) {
            $fileName = time() . '_' . $req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');
            //find element by ID
            $user = Candidat::find($id);
            $user->profile_picture = $filePath;
            $user->save();
            return back()
                ->with('success', 'File has been uploaded.')
                ->with('file', $fileName);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {    //get all the users
        //$users = User::all();
        //load the view and pass the users
        return view('users.index', ['users' => DB::table('candidats')->paginate(5)]);
    }
    //api
    public function allUsers(Request $request)
    {
        $region = $request->query('region');
        $users = [];

        if($region){
            $users = DB::table('candidats')
            ->when($region, function ($query, $region) {
                return $query->where('county', $region);
            })
            ->get();
        } else {
            $users = Candidat::all();
        }
        
        return $users;
    }
    
// public function getUsers(Request $request)
// {
//   $users = User::all();
//   return $users;
// }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // load the view and pass the users
        return view('users.create');
    }

    public function validateUser(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'gender' => 'required',
            'slogan' => 'required',
            'political_party' => 'required',
            'county' => 'required',
            'priority_one' => 'required',
            'priority_two' => 'required',
            'priority_three' => 'required',
            'political_experiences' => 'required',
            'education' => 'required',
            'profession_and_personal_experiences' => 'required',
            'phone_number' => 'required',
            'email' => 'required'

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $formData = $request->all();
        $this->validateUser($request);

        $user = new Candidat();
        // dd($request->all());

        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->gender = $request->input('gender');
        $user->slogan = $request->input('slogan');
        $user->political_party = $request->input('political_party');
        $user->county = $request->input('county');
        $user->priority_one = $request->input('priority_one');
        $user->priority_two = $request->input('priority_two');
        $user->priority_three = $request->input('priority_three');
        $user->political_experiences = $request->input('political_experiences');
        $user->education = $request->input('education');
        $user->profession_and_personal_experiences = $request->input('profession_and_personal_experiences');
        $user->facebook = $request->input('facebook');
        $user->instagram = $request->input('instagram');
        $user->twitter = $request->input('twitter');
        $user->youtube = $request->input('youtube');
        $user->phone_number = $request->input('phone_number');
        $user->email = $request->input('email');
        $user->website = $request->input('website');
        $user->save();

        // Session::flash('message', 'Successfully created user!');
        return redirect('/users/edit/' .  $user->id)->with('status', 'Profile updated!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $counties = array("Tiranë", "Durrës", "Dibër","Elbasan","Fier","Berat","Gjirokastër","Korcë","Kukës","Lezhë","Shkodër","Vlorë");
        $genders = array("femer","mashkull");
        $user = Candidat::find($id);
        return view('users.edit', ['user' => $user, 'counties'=>$counties,'genders'=>$genders]);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $formData = $request->all();

        $user = Candidat::find($id);
        $this->validateUser($request);
        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->gender = $request->input('gender');
        $user->slogan = $request->input('slogan');
        $user->political_party = $request->input('political_party');
        $user->county = $request->input('county');
        $user->priority_one = $request->input('priority_one');
        $user->priority_two = $request->input('priority_two');
        $user->priority_three = $request->input('priority_three');
        $user->political_experiences = $request->input('political_experiences');
        $user->education = $request->input('education');
        $user->profession_and_personal_experiences = $request->input('profession_and_personal_experiences');
        $user->facebook = $request->input('facebook');
        $user->instagram = $request->input('instagram');
        $user->twitter = $request->input('twitter');
        $user->youtube = $request->input('youtube');
        $user->phone_number = $request->input('phone_number');
        $user->email = $request->input('email');
        $user->website = $request->input('website');

        $user->save();

        //Session::flash('message', 'Successfully created user!');
        return redirect('/users')->with('status', 'Profile updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        //delete
        $user = Candidat::find($id);
        $user->delete();
        // dd($user);
        //redirect
        // Session::flash('message','Successfully deleted the user!');
        return Redirect::to("users");
    }
}
