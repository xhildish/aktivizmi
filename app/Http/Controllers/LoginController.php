<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{

        /**
        * Display a listing of the resource.
        *
        * @return Response
        */
        public function index()
        { // get all the users
            // $users = User::all();

            // load the view and pass the users
            return view('auth.login',[]);
        
        }

        
        public function login(Request $request)
        {
            $credentials = $request->only('email', 'password');

            // $request->validate([
            //     'username' => 'required',
            //     'password' => 'required'
            // ]);

            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
    
                return redirect()->intended('users');
            }
    
  
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ]);
        }
    /**
     * Show the profile for a given user.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return view('user.profile', [
            'user' => User::findOrFail($id)
        ]);
    }

    public function  store(Request $request){
        $formData = $request->all();
        dd($formData);
    }
}