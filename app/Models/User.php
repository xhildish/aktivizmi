<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    public $timestamps = false;

    protected $attributes = [
        // 'name',
        // 'surname',
        // 'slogan',
        // 'county',
        // 'priority_one',
        // 'priority_two',
        // 'priority_three',
        // 'political_experiences',
        // 'education',
        // 'profession_and_personal_experiences',
        // 'facebook',
        // 'instagram',
        // 'twitter',
        // 'youtube',
        // 'phone_number',
        // 'email',
        // 'website'
        // 'file_name',
        // 'file_path'
];
    protected $fillable = [
        'name',
        'email',
        'password',
    //     'partia politike',
    //     'qarku',
    //     'prioriteti 1',
    //     'prioriteti 2',
    //     'prioriteti 3',
    //     'pervoja politike',
    //     'arsimi',
    //     'profesioni dhe pervoja personale',
    //     'facebook',
    //     'instagram',
    //     'youtube',
    //     'numer telefoni',
    //     'email',
    //     'website',
    //     'filename',
    //     'file_path'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'password',
    //     'remember_token',
    // ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */


     
    protected $casts = [

    ];
}
