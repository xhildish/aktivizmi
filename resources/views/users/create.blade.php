@extends('layouts.app')
@section('content')
<div class="container-fluid">
<form method="POST" action="{{ url('/users') }}">
    @csrf
    @method('POST')
    <!-- <div class="d-flex justify-content-end header head-cnt">
 <form class="form-inline my-2 my-lg-0">
  <button type="button" class="btn btn-danger">Save</button>
  <button type="button" class="btn btn-danger">Cancel </button>
  </form>
  </div> -->
<div class="buttons d-flex justify-content-end header">
<button type="submit" class="btn btn-danger">Ruaj</button>
<a href="{{ URL::to('/users') }}" type="button"class="btn btn-danger ml-10">Anullo</a>
</div>
<div class="form-row">
<fieldset class="col-lg-12">
    <legend>Te dhenat:</legend>
<div class="row">
<div class ="form-group col-lg-6"> <label for="name">Emri<span class="required-field"></label>
<input placeholder="Emri" id="name"   name="name" value="{{old('name')}}" type="text" class="@error('name') is-invalid @enderror form-control"/>
@error('name')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-6"> <label for="surname">Mbiemri<span class="required-field"></label>
<input placeholder="Mbiemri" id="surname" name="surname" value="{{old('surname')}}" type="text" class="@error('surname') is-invalid @enderror form-control"/>
@error('surname')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-6"> 
<label for="gender">Zgjidhni gjinine<span class="required-field" value="{{old('gender')}}"></label>
  <select id="gender" name="gender" class="form-control">
  <option selected value="null">Gjinia</option>
  <option value="femer">Femer</option>
  <option value="mashkull">Mashkull</option>
  </select>
</div>
<div class ="form-group col-lg-6"> <label for="slogan">Slogani<span class="required-field"></label>
<textarea placeholder="Slogani" id="slogan" name="slogan"  type="text" class="@error('slogan') is-invalid @enderror form-control" maxlength="90">
{{ old('slogan')}}</textarea>
@error('slogan')
  <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
<div class ="form-group col-lg-6"> <label for="political_party">Partia politike<span class="required-field"></label>
<input placeholder="Partia politike" id="political_party" name="political_party" value="{{old('political_party')}}" type="text" class="@error('political_party') is-invalid @enderror form-control"/>
@error('political_party')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
<div class ="form-group col-lg-6"> 
<label for="county">Zgjidhni qarkun<span class="required-field" value="{{old('county')}}"></label>
  <select id="county" name="county" class="form-control">
  <option selected value="null">Qarku</option>
  <option value="Tiranë">Tiranë</option>
  <option value="Durrës">Durrës</option>
  <option value="Dibër">Dibër</option>
  <option value="Elbasan">Elbasan</option>
  <option value="Fier">Fier</option>
  <option value="Berat">Berat</option>
  <option value="Gjirokastër">Gjirokastër</option>
  <option value="Korcë">Korcë</option>
  <option value="Kukës">Kukës</option>
  <option value="Lezhë">Lezhë</option>
  <option value="Shkodër">Shkodër</option>
  <option value="Vlorë">Vlorë</option>
</select>
</div>
<div class ="form-group col-lg-6"> <label for="education">Arsimi<span class="required-field"></label>
<input placeholder="Arsimi" id="education" name="education" value="{{old('education')}}" type="text" class="@error('education') is-invalid @enderror form-control" maxlength="90">
@error('education')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
</div>
</fieldset>
<div class ="form-group col-lg-12"> <label for="prioritetet">Prioritetet kryesore<span class="required-field"></label>
<textarea placeholder="Prioriteti 1" id="priority_one"   name="priority_one"  type="text" class="@error('priority_one') is-invalid @enderror form-control" maxlength="450">
{{old('priority_one')}}</textarea>
@error('priority_one')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-12"> 
<textarea placeholder="Prioriteti 2" id="priority_two" name="priority_two"  type="text" class="@error('priority_two') is-invalid @enderror form-control" maxlength="450">
{{old('priority_two')}}</textarea>

@error('priority_two')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-12"> 
<textarea placeholder = "Prioriteti 3" id="priority_three" name="priority_three"  type="text" class="@error('priority_three') is-invalid @enderror form-control" maxlength="450">
{{old('priority_three')}}</textarea>
@error('priority_three')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-12"> <label for="Pervoja Politike">Pervoja politike<span class="required-field"></label>
<textarea placeholder="Pervoja politike" id="political_experiences"   name="political_experiences"  type="textarea" class="@error('political_experiences') is-invalid @enderror form-control" maxlength="200">
{{old('political_experiences')}}</textarea>
@error('political_experiences')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
<div class ="form-group col-lg-12"> <label for="profession_and_personal_experiences">Profesioni dhe pervoja personale<span class="required-field"></label>
<textarea placeholder="Profesioni dhe pervoja personale"  id="profession_and_personal_experiences"   name="profession_and_personal_experiences" type="textarea" class="@error('profession_and_personal_experiences') is-invalid @enderror form-control" maxlength="450">
{{old('profession_and_personal_experiences')}}</textarea>

@error('profession_and_personal_experiences')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<fieldset class="col-lg-12">
    <legend>Media Sociale:</legend>
<div class="row">


<!-- <div class ="form-group col-lg-6"> <label for="SocialMedia">Media Sociale</label> -->
<div class ="form-group col-lg-6">
<input  placeholder="Facebook" id="facebook" name="facebook" value="{{old('facebook')}}" type="text" class="@error('facebook') is-invalid @enderror form-control" maxlength="90">
@error('facebook')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
<div class ="form-group col-lg-6">
<input placeholder="Instagram" id="instagram"  name="instagram" value="{{old('instagram')}}" type="text" class="@error('instagram') is-invalid @enderror form-control" maxlength="90">
@error('instagram')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-6">
<input placeholder="Twitter"  id="twitter" name="twitter" value="{{old('twitter')}}" type="text" class="@error('twitter') is-invalid @enderror form-control" maxlength="90">
@error('twitter')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div> 
<div class ="form-group col-lg-6">
<input placeholder="Youtube" id="youtube"  name="youtube" value="{{old('youtube')}}" type="text" class="@error('youtube') is-invalid @enderror form-control" maxlength="90">
@error('youtube')
<div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>


</div>
</fieldset>

<fieldset class="col-lg-12">
    <legend>Kontakt:<span class="required-field"></legend>
<div class="row">

<div class ="form-group col-lg-6"> 
<input placeholder="Telefon"  id="phone_number"  name="phone_number" value="{{old('phone_number')}}" type="text " class="@error('phone_number') is-invalid @enderror form-control" maxlength="90"></textarea>
@error('phone_number')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-6"> 
<input placeholder="Email" id="email"  name="email" value="{{old('email')}}" type="email" class="@error('email') is-invalid @enderror form-control" maxlength="90">
   
@error('email')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>

<div class ="form-group col-lg-6"> 
<input placeholder="Website"  id="website"  name="website" value="{{old('website')}}" type="text" class="@error('website') is-invalid @enderror form-control" maxlength="90">
@error('website')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
</div>
</fieldset>
</form>
</div>
</div>
<div class="container mt-5">
            @csrf
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <strong>{{ $message }}</strong>
            </div>
          @endif
          @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
    </form>
</div>
@endsection