@extends('layouts.app')
@section('content')
    <div class="col-12">
        <div class="row">
            <div class="col-12 land">
                <div class="col-4">
                    <form action="{{route('fileUpload', ['id'=>$user->id])}}" method="post" enctype="multipart/form-data" class="upload upload-form">
                        @csrf
                        <fieldset>
                            <img id="blah" class="profile-pic" src="{{url('storage/' . $user->profile_picture)}}" alt="your image" onerror="this.onerror=null;this.src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLZR4nyWw5BedHGciFbwwJIhEu2cjp439L5g&usqp=CAU';" />
                            <div class="custom-file ">
                                <input type="file" name="file" class="custom-file-input" id="chooseFile" onchange="readURL(this);">
                                <label class="custom-file-label" for="chooseFile">Zgjidhni foton</label>
                            </div>

                            <div class="row " id="action-buttons" style="display:none">
                                <div class="form-group col-lg-6">
                                    <button type="submit" name="submit" class="btn btn-danger btn-block mt-4">
                                        Ngarko foton
                                    </button>
                                </div>
                                <div class="form-group col-lg-6">
                                    <button type="button" name="cancel" onClick="reload()" class="btn btn-danger btn-block mt-4">
                                        Anullo
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <div class="col-12">
                <div>
                    <form method="POST" action="{{ url('/users/update/'.$user->id)}}">
                        @csrf
                        @method('POST')

                        <div class="buttons d-flex justify-content-end header">
                            <button type="submit" class="btn btn-danger">Ruaj</button>
                            <a href="{{ URL::to('/users') }}" type="button" class="btn btn-danger ml-10">Anullo</a>
                        </div>
                        <div class="form-row">

                            <fieldset class="col-lg-12">
                                <legend>Te dhenat:</legend>
                                <div class="row">
                                    <div class="form-group col-lg-6"> <label for="name">Emri<span class="required-field"></label>
                                        <input placeholder="Emri" id="name" value="{{$user->name}}" name="name" type="text" class="@error('name') is-invalid @enderror form-control" />
                                        @error('name')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-lg-6"> <label for="surname">Mbiemri<span class="required-field"></label>
                                        <input placeholder="Mbiemri" id="surname" value="{{$user->surname}}" name="surname" type="text" class="@error('surname') is-invalid @enderror form-control" />
                                        @error('surname')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="gender">Zgjidhni gjinine<span class="required-field" value="{{old('gender')}}"></label>
                                        <select id="gender" name="gender" class="form-control">
                                            <option selected>Gjinia</option>
                                            @foreach ($genders as $gender)
                                            <option value="{{ $gender }}" {{ ( strtolower($gender) == strtolower($user->gender)) ? 'selected' : '' }}>
                                                {{ $gender }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-6"> <label for="slogan">Slogani <span class="required-field"></label>
                                        <textarea placeholder="Slogani" id="slogan" name="slogan" type="text" class="@error('slogan') is-invalid @enderror form-control" maxlength="90">{{$user->slogan}}</textarea>
                                        @error('slogan')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-lg-6"> <label for="political_party">Partia politike<span class="required-field"></label>
                                        <input placeholder="Partia politike" id="political_party" value="{{$user->political_party}}" name="political_party" type="text" class="@error('political_party') is-invalid @enderror form-control" />
                                        @error('political_party')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                   
                                    <div class="form-group col-lg-6">
                                        <label for="county">Zgjidhni qarkun<span class="required-field" value="{{old('county')}}"></label>
                                        <select id="county" name="county" class="form-control">
                                            <option selected>Qarku</option>
                                            @foreach ($counties as $county)
                                            <option value="{{ $county }}" {{ ( $county == $user->county) ? 'selected' : '' }}>
                                                {{ $county }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-lg-6"> <label for="education">Arsimi<span class="required-field"></label>
                                        <input placeholder="Arsimi" id="education" value="{{$user->education}}" name="education" type="text" class="@error('education') is-invalid @enderror form-control" maxlength="90">
                                        @error('education')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </fieldset>

                            <div class="form-group col-lg-12"> <label for="prioritetet">Prioritetet kryesore<span class="required-field"></label>
                                <textarea placeholder="Prioriteti 1" id="priority_one" value="{{$user->priority_one}}" name="priority_one" type="text" class="@error('priority_one') is-invalid @enderror form-control" maxlength="450">{{$user->priority_one}}</textarea>
                                @error('priority_one')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-lg-12">
                                <textarea placeholder="Prioriteti 2" id="priority_two" value="{{$user->priority_two}}" name="priority_two" type="text" class="@error('priority_two') is-invalid @enderror form-control" maxlength="450">{{$user->priority_two}}</textarea>
                                @error('priority_two')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-lg-12">
                                <textarea placeholder="Prioriteti 3" id="priority_three" value="{{$user->priority_three}}" name="priority_three" type="text" class="@error('priority_three') is-invalid @enderror form-control" maxlength="450">{{$user->priority_three}}</textarea>
                                @error('priority_three')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group col-lg-12"> <label for="Pervoja Politike">Pervoja politike<span class="required-field"></label>
                                <textarea placeholder="Pervoja politike" id="political_experiences" value="{{$user->political_experiences}}" name="political_experiences" type="textarea" class="@error('political_experiences') is-invalid @enderror form-control" maxlength="200">{{$user->political_experiences}}</textarea>
                                @error('political_experiences')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group col-lg-12"> <label for="profession_and_personal_experiences">Profesioni dhe pervoja personale<span class="required-field"></label>
                                <textarea placeholder="Profesioni dhe pervoja personale" id="profession_and_personal_experiences" value="{{$user->profession_and_personal_experiences}}" name="profession_and_personal_experiences" type="textarea" class="@error('profession_and_personal_experiences') is-invalid @enderror form-control" maxlength="450">{{$user->profession_and_personal_experiences}}</textarea>
                                @error('profession_and_personal_experiences')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <fieldset class="col-lg-12">
                                <legend>Media Sociale:</legend>
                                <div class="row">
                                    <!-- <div class ="form-group col-lg-6"> <label for="SocialMedia">Media Sociale</label> -->
                                    <div class="form-group col-lg-6">
                                        <input placeholder="Facebook" id="facebook" value="{{$user->facebook}}" name="facebook" type="text" class="@error('facebook') is-invalid @enderror form-control" maxlength="90">
                                        @error('facebook')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input placeholder="Instagram" id="instagram" value="{{$user->instagram}}" name="instagram" type="text" class="@error('instagram') is-invalid @enderror form-control" maxlength="90">
                                        @error('instagram')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <input placeholder="Twitter" id="twitter" value="{{$user->twitter}}" name="twitter" type="text" class="@error('twitter') is-invalid @enderror form-control" maxlength="90">
                                        @error('twitter')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <input placeholder="Youtube" id="youtube" value="{{$user->youtube}}" name="youtube" type="text" class="@error('youtube') is-invalid @enderror form-control" maxlength="90">
                                        @error('youtube')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>


                                </div>
                            </fieldset>

                            <fieldset class="col-lg-12">
                                <legend>Kontakt:<span class="required-field"></legend>
                                <div class="row">

                                    <div class="form-group col-lg-6">
                                        <input placeholder="Telefon" id="phone_number" value="{{$user->phone_number}}" name="phone_number" type="text " class="@error('phone_number') is-invalid @enderror form-control" maxlength="90"></textarea>
                                        @error('phone_number')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <input placeholder="Email" id="email" name="email" type="email" value="{{$user->email}}" class="@error('email') is-invalid @enderror form-control" maxlength="90">

                                        @error('email')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="form-group col-lg-6">
                                        <input placeholder="Website" id="website" value="{{$user->website}}" name="website" type="text" class="@error('website') is-invalid @enderror form-control" maxlength="90">
                                        @error('website')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </fieldset>
                    </form>
                </div>
            </div>


        </div>

    </div>
    @endsection
<script>
    function reload() {
        location.reload();
    }

    function readURL(input) {
        const buttons = document.getElementById("action-buttons");
        buttons.style.display = "flex";
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(200)
                    .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

