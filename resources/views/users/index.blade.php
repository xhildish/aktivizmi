
@extends('layouts.app')
@section('content')
    <div>   
        <div class="px-20">
            <div class="row flex">
                <div class="col-sm-6 ">
                    <h1>Kandidatet</h1>

                </div>
                <div class=" col-sm-6 flex-end">
                    <a class="btn btn-danger" href="{{ URL::to('users/create') }}">Krijoni nje kandidat te ri</a>
                </div>
            </div>
            <!-- will be used to show any messages -->
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <td>Emri</td>
                        <td>Mbiemri</td>
                        <td>Slogani</td>
                        <td>Partia Politike</td>
                        <td>Qarku</td>
                        <!-- <td>Prioriteti 1</td>
            <td>Prioriteti 2</td>
            <td>Prioriteti 3</td> -->
                        <td>Pervoja Politike</td>
                        <td>Arsimi</td>
                        <td>Profesioni dhe pervoja personale</td>
                        <td>Email</td>
                        <td>Veprimet</td>
                        <!-- <td>Facebook</td>
            <td>Instagram</td>
            <td>Twitter</td>
            <td>Youtube</td>
            <td>Numer telefoni</td>
            <td>Website</td> -->

                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $value)
                    <tr>
                        <td>{{ $value-> name}}</td>
                        <td>{{ $value-> surname}}</td>
                        <td>{{ $value-> slogan}}</td>
                        <td>{{ $value-> political_party}}</td>
                        <td>{{ $value-> county}}</td>
                        <!-- <td>{{ $value-> priority_one}}</td>
            <td>{{ $value-> priority_two}}</td>
            <td>{{ $value-> priority_three}}</td> -->
                        <td>{{ $value-> political_experiences}}</td>
                        <td>{{ $value-> education}}</td>
                        <td>{{ $value-> profession_and_personal_experiences}}</td>
                        <td>{{ $value-> email}}</td>
                        <!-- <td>{{ $value-> facebook}}</td>
            <td>{{ $value-> instagram}}</td>
            <td>{{ $value-> twitter}}</td>
            <td>{{ $value-> youtube}}</td>
            <td>{{ $value-> phone_number}}</td>
            <td>{{ $value-> website}}</td> -->
                        <!-- we will also add show, edit, and delete buttons -->
                        <td>

                            <!-- delete the shark (uses the destroy method DESTROY /sharks/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->


                            <!-- delete the user (uses the destroy method DESTROY /users/{id} -->
                            <!-- we will add this later since its a little more complicated than the other two buttons -->

                            <!-- show the user (uses the show method found at GET /users/{id} -->
                            <!-- <a class="btn btn-small btn-danger" href="{{ URL::to('users/' . $value->id) }}">Show</a> -->

                            <!-- edit this user (uses the edit method found at GET /users/{id}/edit -->
                            <a class="btn btn-small btn-success" href="{{ URL::to('users'  . '/edit/' . $value->id) }}">Modifiko</a>

                            <!-- delete this user (uses the delete method found at GET /users/{id}/delete -->

                            <a href="#" data-id="{{$value->id}}" class="btn btn-danger delete" data-toggle="modal" data-target="#deleteModal">Fshi</a>

                            <!-- {{ Form::open(array('url' => 'users/' . $value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }} -->

                        </td>
                    </tr>
                    @endforeach
                  </tbody>
               </table>
            <div>
                {{ $users->onEachSide(1)->links() }}
            </div>

            <!-- Delete Warning Modal -->
            <div class="modal modal-danger" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="Delete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Fshini kandidatin</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <form action="/users/delete" method="post">
                            @csrf
                            @method('POST')
                            <div class="modal-body">

                            <input id="id" name="id" style="display: none;">

                            <h5 class="text-center">A jeni te sigurte qe doni te fshini kandidatin?</h5>
                                <!-- <input id="firstName" name="firstName"><input id="lastName" name="lastName"> -->
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Anullo</button>
                            <button type="submit" class="btn btn-sm btn-danger">Po </button>
                            </div>,
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete Modal -->
        <script>
            $(document).on('click', '.delete', function() {
                let id = $(this).attr('data-id');
                $('#id').val(id);
            });
        </script>
    </div>
    @endsection
