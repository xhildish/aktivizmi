<!DOCTYPE html>
<html>

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
</head>

<body>


  <div class="col-sm-12">
    <div class="row d-flex justify-content-center  align-items-center height100">

      <div class="col-sm-4">
        <form method="POST" action="/auth/login" class="nemorf">
          {!! csrf_field() !!}
          <div class="container">
        
              <div class="centerdiv">
                <img src="https://cdn1.iconfinder.com/data/icons/flat-business-icons/128/user-128.png" id="profilepic">
            </div>
            <label for="username"><b>Username</b></label>
            <input type="text" placeholder="Enter Username" name="username" class="@error('username') is-invalid @enderror form-control">
            @error('username')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <label for="password"><b>Password</b></label>
            <input type="password" placeholder="Enter Password" name="password" class="@error('password') is-invalid @enderror form-control">
            @error('password')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            <button class="loginbutton" type="submit">Login</button>
            <label>
              <input type="checkbox" checked="checked" name="remember"> Remember me
            </label>
          </div>

          <div class="login-footer col-12">

            <label>
              <p name="remember"> Don't you have an account with us? </p>
              <button type="button" class="signupbtn">Sign Up</button>
            </label>

            <span class="password">Forgot <a href="#">password?</a></span>
          </div>
        </form>

      </div>
    </div>
  </div>


</body>

</html>