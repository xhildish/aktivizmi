<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/auth', [LoginController::class,'index'])->name('auth');
// Route::post('/auth/login', [LoginController::class,'login']);
Route::post('/users', [UserController::class, 'store'])->middleware('auth');;
Route::post('/users/update/{id}', [UserController::class, 'update'])->middleware('auth');;
Route::get('/users/create', [UserController::class, 'create'])->middleware('auth');;
Route::get('/users/edit/{id}', [UserController::class, 'edit'])->middleware('auth');;
Route::get('/users', [UserController::class, 'index'])->middleware('auth');;
Route::get('/', [UserController::class, 'index'])->middleware('auth');;
Route::post('/users/delete', [UserController::class, 'destroy'])->middleware('auth');;
Route::post('/upload-file', [UserController::class, 'fileUpload'])->name('fileUpload');
Auth::routes();

